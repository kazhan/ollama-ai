+++
title = "Ollama : IA et modèles de langages"
outputs = ["Reveal"]
[logo]
src = "Logo.png"
+++

![](ollama.png)

# Ollama

IA et modèles de langages

---

## Table des matières

* {{< frag c="Rappel sur les Intelligences Artificielles" >}}
* {{< frag c="Les modèles de language" >}}
* {{< frag c="Présentation du projet Ollama" >}}
* {{< frag c="Exemple dans la pratique" >}}
* {{< frag c="Contraintes matérielles" >}}
* {{< frag c="Conclusion" >}}


---

{{% section %}}

## Rappel sur l'IA

<div style="font-size : 75%;font-family : cursive;">
Donne-moi une définition de l'intelligence artificielle.
</div>

<div style="font-size : 50%; font-family : monospace;">

L'Intelligence Artificielle (IA) est un domaine de l'informatique qui vise à créer des systèmes capables de réaliser des tâches qui, à l'origine, nécessitaient l'intelligence humaine.

Il s'agit donc de concevoir des algorithmes et des modèles informatiques pour simuler des fonctions cognitives telles que l'apprentissage, la résolution de problèmes, la reconnaissance d'images ou de discours, la prise de décision, etc. 

L'IA peut être classée en deux catégories : l'IA faible, conçue pour effectuer une tâche spécifique, et l'IA forte, capable d'effectuer n'importe quelle tâche intellectuelle qu'un humain peut faire.
</div>

<div style="font-size : 30%;font-family : fantasy;"> - Mistral - </div>

---

## Rappel sur l'IA

* Simulation de l'intelligence humaine
* Vise à faciliter la résolution de problèmes
* Se reposant sur 2 sous-domaines :
    * _Machine Learning_
    * _Deep Learning_
* Réseaux de neurones

{{% /section %}}

---

{{% section %}}

## Les modèles de langages

<div style="font-size : 75%;font-family : cursive;">
Qu'est-ce qu'un modèle de langage dans le domaine de l'IA ?
</div>

<div style="font-size : 30%; font-family : monospace;">

En Intelligence Artificielle, un modèle de langage est un algorithme ou un ensemble d'algorithmes qui permet de traiter et de comprendre le langage naturel. Ces modèles sont utilisés pour permettre aux ordinateurs de comprendre et de générer du langage naturel, de manière à ce qu'ils puissent interagir avec les humains de manière plus naturelle et plus efficace.

Les modèles de langage en Intelligence Artificielle sont souvent basés sur l'apprentissage automatique, ce qui signifie qu'ils sont entraînés à partir de vastes quantités de données de langage naturel. Ces données peuvent inclure des textes, des conversations, des audio ou des vidéos, et ils sont utilisés pour enseigner aux ordinateurs à reconnaître et à comprendre les différentes structures et les caractéristiques du langage naturel.

Les modèles de langage sont utilisés dans de nombreux domaines de l'Intelligence Artificielle, tels que le traitement du langage naturel (NLP), la génération de langage naturel, la compréhension de la parole, la synthèse vocale, la traduction automatique, le dialogue naturel et la résolution de problèmes.

Il existe différents types de modèles de langage en Intelligence Artificielle, tels que les modèles de langage statistique, les modèles de langage basés sur des règles, les modèles de langage hybrides et les modèles de langage cognitifs. Chacun de ces modèles a des avantages et des inconvénients, et ils sont utilisés dans différents contextes et pour différents objectifs.

Les modèles de langage sont très utiles dans de nombreux domaines, tels que la gestion de la clientèle, la réponse à des questions fréquentes, la fourniture de services de conversation, la réalisation de tâches de traitement du langage naturel, la compréhension de la parole et la synthèse vocale. Ils sont également utilisés dans les assistants virtuels, tels que Amazon Alexa, Google Assistant et Apple Siri, pour permettre aux utilisateurs de communiquer avec ces assistants de manière plus naturelle et plus efficace.

</div>

<div style="font-size : 30%;font-family : fantasy;"> - Llama2 (70b) - </div>

---

## Les modèles de langage

* Modèle statistique
* Traitement du langage naturel
* Plusieurs domaines d'application
    * Assistants virtuels
    * Gestion de la clientelle
    * Services de conversation

---

## Les modèles de langage

Quelques points clés :
* __Apprentissage__ - Formés sur de grands corpus de textes (Livres, discussions, encyclopédies, ...)
* __Prédiction__ - Peuvent prédire les mots dans une phrase donnée
* __Génération de texte__ - Peut générer du texte comme une ~~dissertation~~ histoire
* __Applications__ - Peuvent êtres utilisées pour la traduction automatique, les assistants virtuels, ...


{{% /section %}}

---
## Le projet Ollama

* {{< frag c="Rendre disponible des modèles de langages" >}}
* {{< frag c="Personnalisation des modèles" >}}
* {{< frag c="Fonctionnement similaire à Docker" >}}
* {{< frag c="Interface Web : Open WebUI" >}}

---

## Mise en pratique

---

## Contraintes matérielles
* {{< frag c="Vitesse de traitement dépendant du matériel" >}}
* {{< frag c="Demande de la mémoire vive (beaucoup...)" >}}
* {{< frag c="Fonctionne sur des cartes graphique ou processeurs puissants" >}}

---

## Conclusion
{{< mermaid >}}
mindmap
  root((Ollama))
        Llama2
            (7b)
            (13b)
            (70b)
            (chat)
        
        Codellama
            (7b)
            (13b)
            (34b)
            (70b)
            (python)

        Mistral
            (7b)
            (text)

        Mixtral
            (text)
            (8x7b)
            (instruct)

{{< /mermaid >}}